package com.example.menghao.task2;

/**
 * Created by menghao on 17/02/16.
 */
public class DataObject {
    String text;
    int img;

    public DataObject(String text, int img) {
        this.text = text;
        this.img = img;
    }

    public String getText() {
        return text;
    }

    public int getImg() {
        return img;
    }
}
