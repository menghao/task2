package com.example.menghao.task2;


import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

/**
 * Created by menghao on 17/02/16.
 */
public class RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    List<DataObject> object;
    public RVAdapter(List<DataObject> object) {
        this.object = object;
    }

    public static class CustomViewHolder1 extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView img;
        public CustomViewHolder1(View itemView) {
            super(itemView);
            this.img = (ImageView) itemView.findViewById(R.id.item_image);
            this.img.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent i = new Intent(v.getContext(), Detail0Activity.class);
            i.putExtra("image", (Integer) this.img.getTag());
            v.getContext().startActivity(i);
        }
    }

    public static class CustomViewHolder2 extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView text;
        public CustomViewHolder2(View itemView) {
            super(itemView);
            this.text = (TextView) itemView.findViewById(R.id.item_text);
            this.text.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent i = new Intent(v.getContext(), Detail1Activity.class);
            i.putExtra("text", this.text.getText());
            v.getContext().startActivity(i);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position%2;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case 0:{
                View v  = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row0, parent, false);
                CustomViewHolder1 customViewHolder1 = new CustomViewHolder1(v);
                return customViewHolder1;
            }
            case 1:{
                View v  = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row1, parent, false);
                CustomViewHolder2 customViewHolder2 = new CustomViewHolder2(v);
                return customViewHolder2;
            }
            default:return null;
        }
    }

    @Override
    public int getItemCount() {
        return object.size();
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int i = holder.getItemViewType()%2;
        if (i == 0 ){
            CustomViewHolder1 holder1 = (CustomViewHolder1) holder;
            holder1.img.setImageResource(object.get(position).getImg());
            holder1.img.setTag(object.get(position).getImg());

        }else{
            CustomViewHolder2 holder2 = (CustomViewHolder2) holder;
            holder2.text.setText(object.get(position).getText());
        }
    }
}
