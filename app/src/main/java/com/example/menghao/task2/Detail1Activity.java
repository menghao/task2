package com.example.menghao.task2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Detail1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_row1);
        Bundle extas = getIntent().getExtras();
        if (extas != null) {
            String text = extas.getString("text");
            TextView item_text = (TextView) findViewById(R.id.item_text);
            item_text.setText(text);
        }

    }
}