package com.example.menghao.task2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class Detail0Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_row0);
        Bundle extas = getIntent().getExtras();
        if (extas != null) {
            int img = extas.getInt("image");
            ImageView item_image = (ImageView) findViewById(R.id.item_image);
            item_image.setImageResource(img);
        }

    }
}
